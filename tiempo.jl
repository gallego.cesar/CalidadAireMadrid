using HTTP, JSON, DataFrames
import HTTP.URIs: escapeuri

function jsontodf(a)
    ka = union([keys(r) for r in a]...)
    df = DataFrame(;Dict(Symbol(k)=>get.(a,k,"") for k in ka)...)
    return df
end

function cached_req(url, fil)
    if !isfile(fil)
        meta = HTTP.request("GET", url)
        parsed_meta = JSON.parse(String(meta.body))
        data = HTTP.request("GET", parsed_meta["datos"])
        open(fil, "w") do file
            write(file, String(data.body))
        end
        content = JSON.parse(String(data.body))
    else
        open(fil, "r") do file
            content = JSON.parse(read(file, String))
        end
    end
    content
end

base = "https://opendata.aemet.es/opendata"
listado_estaciones = "/api/valores/climatologicos/inventarioestaciones/todasestaciones"

println("Plese provide you Open Data AEMET key")
key = readline(stdin)

estaciones = cached_req("$base$listado_estaciones?api_key=$key", "estaciones.json")
df = jsontodf(estaciones)
madrid = df[df[:provincia] .== "MADRID", :]
#for indicativo in madrid[:indicativo]
#end
contamination_dates = [
    escapeuri("2001-01-01T00:00:00UTC"),
    escapeuri("2004-01-01T00:00:00UTC"),
    escapeuri("2007-01-01T00:00:00UTC"),
    escapeuri("2010-01-01T00:00:00UTC"),
    escapeuri("2013-01-01T00:00:00UTC"),
    escapeuri("2016-01-01T00:00:00UTC"),
    escapeuri("2019-01-01T00:00:00UTC"),
    escapeuri("2019-04-01T00:00:00UTC"),
]
queries = zip(contamination_dates[1:end-1], contamination_dates[2:end])
for (fini, ffin) in queries
    println(fini, ffin)
end
#estacion = "3195"
#example_url = "$base/api/valores/climatologicos/diarios/datos/fechaini/$fini/fechafin/$ffin/estacion/$estacion?api_key=$key"
#res = HTTP.request("GET", example_url)
#println(res)
#println(String(res.body))