using JSON, Distributed, Base.Filesystem
# get list of files
urls = JSON.parsefile("data_urls.json")
addprocs(3)

@everywhere using Base.Filesystem

@everywhere function getDataset(current)
  zip_file = download_data(current)
  year_dir = unzip_data(current["file"], zip_file)
  find_text_data(year_dir)
end

# download_data
@everywhere function download_data(current)
  name = current["file"]
  target = "$(name).zip"
  file = joinpath("data", target)
  dir = current["url"]
  if !isfile(file)
    println("[Download] f: $file, d: $dir")
    download(current["url"],  file)
  else
    println("[Skip] f: $file, d: $dir")
  end
  file
end

# unzip data
@everywhere function unzip_data(name, file)
  target = joinpath("data", name)

  cmd = `7z x -y -o$target $file`
  run(cmd)
  target
end

# find Text files
@everywhere function find_text_data(folder)
  data_files = String[]
  pattern = r".txt$"
  for (root, dirs, files) in walkdir(folder)
    for current in files
      if occursin(pattern, current)
        push!(data_files, joinpath(root, current))
      end
    end
  end
  return data_files
end

# create target directory
if !isdir("data")
  mkdir("data")
end

# extract fields
function extract_fields(file)
  rows = Channel(1)
  @async begin
    f = open(file)
    for l in readlines(f)
      put!(rows, l)
    end
    close(f)
    close(rows)
  end
  rows
end

# positional split
function pos_split(cuts, target)
  out = String[]
  start = 1
  finish = 0
  for cut in cuts
    finish += cut
    push!(out, target[start:finish])
    start = finish+1
  end
  out
end

# convert to expected fields
function line2struct(line)
  if occursin(",", line)
    return split(line, ",")
  elseif occursin(r"[VMZNC]+", line)
    pospart, splpart = line[1:21], line[21:end]
    out = pos_split([2, 3, 3, 2, 2, 2, 2, 2, 2], pospart)
    for (val, flag) in zip(split(splpart, r"[VMZNC]+")[1:end-1], split(splpart, r"[^VMZNC]+")[2:end])
      push!(out, val)
      push!(out, flag)
    end
    return out
  else
    return line
  end
end


const headers = ["PROVINCIA","MUNICIPIO","ESTACION","MAGNITUD", "TECNICA", "HORARIO", "ANO","MES","DIA","H01","V01","H02","V02","H03","V03","H04","V04","H05","V05","H06","V06","H07","V07","H08","V08","H09","V09","H10","V10","H11","V11","H12","V12","H13","V13","H14","V14","H15","V15","H16","V16","H17","V17","H18","V18","H19","V19","H20","V20","H21","V21","H22","V22","H23","V23","H24","V24"];
data_files = collect(Iterators.flatten(pmap(getDataset, urls)))
open("contaminacion.csv", "w") do file
  write(file, join(headers, ";"), "\n")
  for f in data_files
    current_file_content = extract_fields(f)
    for l in current_file_content
      row = line2struct(l)
      write(file, join(row, ";"), "\n")
    end
  end
end