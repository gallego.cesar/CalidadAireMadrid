Calidad del aire en madrid
==========================
Fuentes de datos: 
 - Estaciones de lectura de Contaminación: https://datos.madrid.es/portal/site/egob/menuitem.c05c1f754a33a9fbe4b2e4b284f1a5a0/?vgnextoid=f3c0f7d512273410VgnVCM2000000c205a0aRCRD&vgnextchannel=374512b9ace9f310VgnVCM100000171f5a0aRCRD#
 - Localizaciones estaciones de Contaminación: https://datos.madrid.es/portal/site/egob/menuitem.c05c1f754a33a9fbe4b2e4b284f1a5a0/?vgnextoid=9e42c176313eb410VgnVCM1000000b205a0aRCRD&vgnextchannel=374512b9ace9f310VgnVCM100000171f5a0aRCRD&vgnextfmt=default
 - Localización estaciones metereologícas: https://opendata.aemet.es/opendata/api/valores/climatologicos/inventarioestaciones/todasestaciones

Consideraciones Sobre Contaminación
-----------------------------------
La descripción que ofrece el catálogo es, cuanto menos incompleta, hay más valores de separación a parte de N y V (como C, M y N). En un caso un dato tiene N y V al mismo tiempo, no siendo un caracter de seperación único.

Nos encontramos la siguiente aclaración al respecto en los comentarios:
```
El significado del os códigos de validación para cada dato es V: dato válido 
N: dato no válido M: en mantenimiento C: en calibración multipunto Z: en 
calibración cero-span Los únicos datos que han de considerarse son los 
marcados con "V"
```

Hay datos precalculados mezclados con las observaciones, hay que ir a los comentarios para encontrar:

```
Hasta el año 2010, se incluía en los ficheros los datos de la media de todas
 las estaciones que configuran la Red de Vigilancia de la Calidad del Aire,
 con el código 28079099, a partir del año 2011 no se publican al no tener este
 valor medio carácter normativo
```

Hay métodos de medición obsoletos que tampoco están en la documentación, hay que ir a los comentarios para encontrar:

```
Respecto a la técnica de análisis 51 que correspondía a la cromatografía 
iónica indicar que no se utiliza en la actualidad en la Red de Vigilancia de 
la Calidad del Aire de Madrid, al igual que la magnitud 58 que se correspondía 
con el ácido clorhídrico, de la cual no se registran datos actualmente. En 
ambos casos se pueden encontrar datos en los ficheros antiguos pero no en los 
actuales.
```

Los datos horarios estan en hora local, no en formato informático, esto dificulta su procesado y además solo está documentado (de nuevo) en los comentarios:

```
Todos los datos están en hora local correspondiendo al horario oficial de España 
CET (Central European Time), es decir, hora solar + 2 (verano) desde último 
domingo de marzo a último domingo de octubre y +1 (invierno) desde último domingo 
de octubre a último domingo de marzo.
```

En el dataset de estaciones están solo las actuales, en la documentación nos encontramos con códigos que han cambiado a lo largo del tiempo.

La tabla de Magnitudes, Técnicas y únidades de medida se incluye en la documentación pero no en el set de datos. Por lo que no se puede automatizar su uso.

Consideraciones sobre AEMET
---------------------------
AEMET usa un método de registro por el que tienes que darte de alta y te darán una clave para operar. El API está bien documentada y los datos son constantes y homogeneos.
